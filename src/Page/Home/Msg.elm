module Page.Home.Msg exposing (Msg(..))

import Http
import Data.Product exposing (Product)
import Data.ShoutOutMessage exposing (ShoutOutMessage)


type Msg
    = ReceiveProducts (Result Http.Error { products : List Product, message : Maybe ShoutOutMessage })
    | AddProductToShoppingCart Product
