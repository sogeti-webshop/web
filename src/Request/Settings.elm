module Request.Settings exposing (apiUrl, orderServiceUrl, clientId)


apiUrl : String
apiUrl =
    "http://api:8081"


orderServiceUrl : String
orderServiceUrl =
    "http://orderservice:8082"


clientId : String
clientId =
    "dbc94d04-fa51-4db6-9170-f45194f10ab5"
