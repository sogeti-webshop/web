module Data.Validation exposing (decodeErrors)

import Json.Decode as Decode exposing (Decoder, field, string, decodeString)


splitErrors : String -> List String
splitErrors error =
    String.split "," error


errorDecoder : Decoder String
errorDecoder =
    (field "message" string)


decodeErrors : String -> List String
decodeErrors jsonErrors =
    case (decodeString errorDecoder jsonErrors) of
        Ok val ->
            splitErrors val

        Err _ ->
            []
