module Request.Products exposing (getProducts)

import Data.Product exposing (Product)
import Http
import HttpBuilder exposing (..)
import Request.Settings exposing (apiUrl)
import Json.Decode


getProducts : Http.Request (List Product)
getProducts =
    apiUrl
        ++ "/product/all"
        |> HttpBuilder.get
        |> HttpBuilder.withExpect (Http.expectJson (Json.Decode.list Data.Product.decoder))
        |> HttpBuilder.toRequest
