module Data.Product exposing (Product, decoder, encode, ProductId)

import Json.Decode exposing (Decoder, string, float, int)
import Json.Decode.Pipeline exposing (decode, required)
import Json.Encode as Encode exposing (Value)


type alias ProductId =
    Int


type alias Product =
    { id : ProductId
    , name : String
    , description : String
    , price : Float
    }


decoder : Decoder Product
decoder =
    decode Product
        |> required "id" int
        |> required "name" string
        |> required "description" string
        |> required "price" float


encode : Product -> Value
encode product =
    Encode.object
        [ ( "id", Encode.int product.id )
        , ( "name", Encode.string product.name )
        , ( "description", Encode.string product.description )
        , ( "price", Encode.float product.price )
        ]
