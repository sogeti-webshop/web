module Route exposing (Route(..), href, fromLocation, fixLocationQuery, modifyUrl)

import UrlParser as Url exposing (parseHash, s, (</>), (<?>), string, oneOf, Parser)
import Navigation exposing (Location)
import Html exposing (Attribute)
import Html.Attributes as Attr


-- ROUTING --


type Route
    = Home (Maybe String) (Maybe String)
    | Register
    | StartLogin
    | Login (Maybe String) (Maybe String) (Maybe String)
    | Logout
    | ShoppingCart


route : Parser (Route -> a) a
route =
    oneOf
        [ Url.map Home (s "" <?> Url.stringParam "access_token" <?> Url.stringParam "message")
        , Url.map Register (s "register")
        , Url.map StartLogin (s "startLogin")
        , Url.map Login (s "login" <?> Url.stringParam "client_id" <?> Url.stringParam "redirect_uri" <?> Url.stringParam "response_type")
        , Url.map Logout (s "logout")
        , Url.map ShoppingCart (s "shoppingcart")
        ]


modifyUrl : Route -> Cmd msg
modifyUrl =
    routeToString >> Navigation.newUrl



-- INTERNAL --


routeToString : Route -> String
routeToString page =
    let
        pieces =
            case page of
                Home _ (Just message) ->
                    [ "?message=" ++ message ]

                Home _ Nothing ->
                    [ "" ]

                Register ->
                    [ "register" ]

                StartLogin ->
                    [ "startLogin" ]

                Login maybeClientId maybeRedirectUrl maybeResponseType ->
                    case ( maybeClientId, maybeRedirectUrl, maybeResponseType ) of
                        ( Just id, Just url, Just responseType ) ->
                            [ "login?" ++ id ++ "&" ++ url ++ "&" ++ responseType ]

                        ( _, _, _ ) ->
                            [ "notfound" ]

                Logout ->
                    [ "logout" ]

                ShoppingCart ->
                    [ "shoppingcart" ]
    in
        "#/" ++ (String.join "/" pieces)



-- PUBLIC HELPERS --


href : Route -> Attribute msg
href route =
    Attr.href (routeToString route)


fromLocation : Location -> Maybe Route
fromLocation location =
    if String.isEmpty location.hash then
        Just (Home Nothing Nothing)
    else
        parseHash route location


fixLocationQuery : Location -> Location
fixLocationQuery location =
    let
        hash =
            String.split "?" location.hash
                |> List.head
                |> Maybe.withDefault ""

        search =
            String.split "?" location.hash
                |> List.drop 1
                |> String.join "?"
                |> String.append "?"
    in
        { location | hash = hash, search = search }
