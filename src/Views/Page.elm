module Views.Page exposing (frame)

import Html exposing (Html, nav, div, a, text, ul, li, i, span)
import Html.Attributes exposing (class, id, attribute, style)
import Route exposing (Route(..))


-- When someone is not logged in, the Maybe String will be a Nothing
-- This will be used to determine whether someone is logged in or not


frame : Maybe String -> Int -> Html msg -> Html msg
frame maybeFullName totalQuantityInShoppingCart content =
    div []
        [ navbar maybeFullName totalQuantityInShoppingCart
        , div [ class "container" ] [ content ]
        ]


navbar : Maybe String -> Int -> Html msg
navbar maybeFullName totalQuantityInShoppingCart =
    let
        shoppingCartMenuItem =
            li []
                [ a [ Route.href Route.ShoppingCart ]
                    [ div []
                        [ i [ class "fa fa-shopping-cart", attribute "aria-hidden" "true" ] []
                        , div [ class "shopping-cart_number-of-items" ] [ text (toString totalQuantityInShoppingCart) ]
                        ]
                    ]
                ]

        rightMenuItems =
            case maybeFullName of
                Just fullName ->
                    [ li []
                        [ span []
                            [ text ("Welkom, " ++ fullName) ]
                        ]
                    , li []
                        [ a
                            [ Route.href Route.Logout ]
                            [ text "Uitloggen" ]
                        ]
                    ]

                Nothing ->
                    [ li []
                        [ a [ Route.href Route.StartLogin ] [ text "Inloggen" ]
                        ]
                    , li []
                        [ a [ Route.href Route.Register ] [ text "Registreren" ]
                        ]
                    ]

        menu =
            shoppingCartMenuItem :: rightMenuItems
    in
        nav []
            [ div [ class "nav-wrapper" ]
                [ a [ class "left brand-logo", Route.href (Route.Home Nothing Nothing) ]
                    [ text "Pet Supplies" ]
                , ul [ class "items right" ]
                    menu
                ]
            ]
