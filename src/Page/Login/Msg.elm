module Page.Login.Msg exposing (Msg(..))

import Http exposing (Error)
import Data.AccessToken exposing (AccessToken)


type Msg
    = TryLogin
    | UpdateEmail String
    | UpdatePassword String
    | LoginResult (Result Http.Error AccessToken)
