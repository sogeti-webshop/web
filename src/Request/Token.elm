module Request.Token exposing (getToken, setToken, deleteToken)

import Request.Storage as Storage exposing (StorageError(..))
import Data.AccessToken exposing (AccessToken, encodeAccessToken)
import Task exposing (Task)
import Json.Decode exposing (string)


accessTokenStorageKey : String
accessTokenStorageKey =
    "access_token"


getToken : Task StorageError (Maybe AccessToken)
getToken =
    Storage.get accessTokenStorageKey string


setToken : String -> Task StorageError ()
setToken token =
    encodeAccessToken token
        |> Storage.set accessTokenStorageKey


deleteToken : Task StorageError ()
deleteToken =
    Storage.delete accessTokenStorageKey
