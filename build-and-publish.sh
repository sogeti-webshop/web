#!/bin/bash

elm-make ./src/Main.elm --yes && docker build -t registry.gitlab.com/sogeti-webshop/web . && docker push registry.gitlab.com/sogeti-webshop/web
