module Views.Login exposing (view)

import Html exposing (Html, text, div, input, label, button)
import Html.Attributes exposing (id, class, type_, required, value, for, name)
import Html.Events exposing (onInput, onClick)
import Page.Login.Msg exposing (Msg(..))
import Data.ShoutOutMessage exposing (ShoutOutMessage)
import Views.ShoutOutBanner as ShoutOutBanner


view : String -> String -> Maybe ShoutOutMessage -> Html Msg
view email password shoutOutMessage =
    let
        shoutOutBanner =
            case shoutOutMessage of
                Just message ->
                    ShoutOutBanner.view message

                Nothing ->
                    text ""
    in
        div [ id "login-form", class "row" ]
            [ shoutOutBanner
            , div [ class "col s12 m7 l6" ]
                [ div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ onInput UpdateEmail, id "email", type_ "email", required True, class "validate", value email ] []
                        , label [ for "email" ] [ text "E-mailadres" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ onInput UpdatePassword, id "password", type_ "password", required True, class "validate", value password ] []
                        , label [ for "password" ] [ text "Wachtwoord" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "col right" ]
                        [ button
                            [ onClick TryLogin, class "btn waves-effect waves-light", name "action" ]
                            [ text "Inloggen" ]
                        ]
                    ]
                ]
            ]
