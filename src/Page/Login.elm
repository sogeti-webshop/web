module Page.Login exposing (Model, initialModel, update, view)

import Html exposing (Html, text)
import Views.Login exposing (view)
import Page.Login.Msg exposing (Msg(..))
import Http exposing (toTask, Error)
import Request.User
import Task exposing (Task)
import Navigation exposing (load)
import Data.ShoutOutMessage exposing (ShoutOutMessage, Status(Danger))


type alias Model =
    { email : String
    , password : String
    , redirectUrl : String
    , shoutOutBanner : Maybe ShoutOutMessage
    }


initialModel : String -> Model
initialModel redirectUrl =
    { email = ""
    , password = ""
    , redirectUrl = redirectUrl
    , shoutOutBanner = Nothing
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateEmail newEmail ->
            { model | email = newEmail } ! []

        UpdatePassword newPassword ->
            { model | password = newPassword } ! []

        TryLogin ->
            let
                task =
                    Http.toTask (Request.User.login model.email model.password)
            in
                model ! [ Task.attempt LoginResult task ]

        LoginResult result ->
            case result of
                Err http ->
                    ( { model
                        | shoutOutBanner =
                            (Just
                                { status = Danger
                                , title = "Er ging iets fout bij het inloggen"
                                , shoutOutItems = [ "Wij kunnen de opgegeven combinatie van e-mailadres en wachtwoord niet vinden." ]
                                }
                            )
                      }
                    , Cmd.none
                    )

                Ok accessToken ->
                    ( model, load (model.redirectUrl ++ "?access_token=" ++ accessToken) )


view : Model -> Html Msg
view model =
    Views.Login.view model.email model.password model.shoutOutBanner
