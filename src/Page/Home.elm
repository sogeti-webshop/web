module Page.Home exposing (..)

import Html exposing (Html, div, text)
import Http
import Http exposing (Error)
import Page.Home.Msg exposing (Msg(..))
import Data.Product exposing (Product)
import Views.Products exposing (productsView)
import Views.ShoutOutBanner as ShoutOutBanner
import Request.Products exposing (getProducts)
import Data.ShoutOutMessage exposing (ShoutOutMessage, successShoutOutMessage)
import Task exposing (..)


type alias Model =
    { products : List Product
    , message : Maybe ShoutOutMessage
    }


model : List Product -> Maybe String -> Model
model products maybeMessage =
    { products = products, message = Maybe.map successShoutOutMessage maybeMessage }


initialModel : Model
initialModel =
    { products = []
    , message = Nothing
    }


init : Maybe String -> Task Error Model
init maybeMessage =
    let
        shoutOutMessage =
            Maybe.map successShoutOutMessage maybeMessage
    in
        Http.toTask getProducts
            |> Task.map (\products -> { products = products, message = shoutOutMessage })


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ReceiveProducts result ->
            case result of
                Err httpError ->
                    ( model, Cmd.none )

                Ok products ->
                    ( products, Cmd.none )

        AddProductToShoppingCart product ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    let
        shoutOutBanner =
            Maybe.map ShoutOutBanner.view model.message
                |> Maybe.withDefault (text "")
    in
        div []
            [ shoutOutBanner
            , productsView model.products AddProductToShoppingCart
            ]
