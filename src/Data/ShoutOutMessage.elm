module Data.ShoutOutMessage exposing (Status(..), ShoutOutMessage, errorShoutOutMessage, successShoutOutMessage)


type Status
    = Success
    | Danger


type alias ShoutOutMessage =
    { status : Status
    , title : String
    , shoutOutItems : List String
    }


errorShoutOutMessage : String -> List String -> ShoutOutMessage
errorShoutOutMessage title shoutOutItems =
    { status = Danger
    , title = title
    , shoutOutItems = shoutOutItems
    }


successShoutOutMessage : String -> ShoutOutMessage
successShoutOutMessage title =
    { status = Success
    , title = title
    , shoutOutItems = []
    }
