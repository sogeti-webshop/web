module Views.Products exposing (..)

import Html exposing (Html, text, div, h5, span, p, a)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Data.Product exposing (Product)


productsView : List Product -> (Product -> msg) -> Html msg
productsView products addProductToCart =
    div [ class "row" ] (List.map (productView addProductToCart) products)


productView : (Product -> msg) -> Product -> Html msg
productView addProductToCart product =
    div [ class "col s12 m6 l4" ]
        [ div [ class "card" ]
            [ div [ class "product card-content" ]
                [ span [ class "card-title" ] [ text product.name ]
                , p [ class "product-overview_description" ] [ text product.description ]
                , h5 [ class "product-overview_price" ] [ text ("€" ++ (toString product.price)) ]
                , a [ class "product-overview_add-to-cart btn waves-effect waves-light", onClick (addProductToCart product) ] [ text "Toevoegen" ]
                ]
            ]
        ]
