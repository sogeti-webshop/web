module Data.Order exposing (Order, encode)

import Data.ShoppingCart as ShoppingCart exposing (ShoppingCart)
import Json.Encode as Encode
import Data.Details as Details exposing (OrderDetails)


type alias Order =
    { orderDetails : OrderDetails
    , shoppingCart : ShoppingCart
    }


encode : Order -> Encode.Value
encode order =
    Encode.object
        [ ( "orderDetails", Details.encode order.orderDetails )
        , ( "shoppingCart", ShoppingCart.encodeShoppingCartProductIdsAndQuantities order.shoppingCart )
        ]
