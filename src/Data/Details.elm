module Data.Details exposing (PersonalData, OrderDetails, decoderDetails, encode)

import Json.Encode as Encode
import Json.Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (decode, required, optional)


type alias Details =
    { email : String
    , fullName : String
    , address : String
    , postalCode : String
    , residence : String
    }


type alias PersonalData =
    Details


type alias OrderDetails =
    Details


decoderDetails : Decoder Details
decoderDetails =
    decode Details
        |> required "email" string
        |> required "fullName" string
        |> required "address" string
        |> required "postalCode" string
        |> required "residence" string


encode : Details -> Encode.Value
encode details =
    Encode.object
        [ ( "email", Encode.string details.email )
        , ( "fullName", Encode.string details.fullName )
        , ( "address", Encode.string details.address )
        , ( "postalCode", Encode.string details.postalCode )
        , ( "residence", Encode.string details.residence )
        ]
