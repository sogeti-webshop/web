module Main exposing (main)

import Navigation exposing (Location)
import Route exposing (Route)
import Page.Home as Home
import Page.Login as Login
import Page.Register as Register
import Page.ShoppingCart as ShoppingCart exposing (Msg(RemoveProductFromShoppingCart, ChangeQuantityOfProductInShoppingCart, OrderPlaced))
import Page.Login.Msg as LoginMsgs
import Page.Register.Msg as RegisterMsgs
import Page.Home.Msg as HomeMsgs exposing (Msg(AddProductToShoppingCart))
import Page.NotFound as NotFound
import Views.Page as Page
import Data.User as User exposing (User)
import Data.ShoppingCart exposing (ShoppingCart, addProductToShoppingCart, removeProductFromShoppingCart, changeQuantityOfProductInShoppingCart)
import Request.User exposing (initLogin, getUserFromToken, logout)
import Request.ShoppingCart exposing (getShoppingCart, setShoppingCart, deleteShoppingCart)
import Request.Token exposing (getToken, setToken, deleteToken)
import Request.Storage exposing (StorageError)
import Data.AccessToken exposing (AccessToken)
import Navigation
import Html exposing (..)
import Route exposing (Route(..), modifyUrl)
import Task exposing (attempt)
import Http exposing (Error(BadStatus))


type Page
    = NotFound
    | Home Home.Model
    | Register Register.Model
    | Login Login.Model
    | ShoppingCart ShoppingCart.Model



-- MODEL --


type alias Model =
    { activePage : Page
    , accessToken : Maybe AccessToken
    , activeUser : Maybe User
    , shoppingCart : ShoppingCart
    }


init : Location -> ( Model, Cmd Msg )
init location =
    let
        ( model, cmd ) =
            setRoute ((Route.fixLocationQuery >> Route.fromLocation) location)
                { activePage = initialPage
                , accessToken = Nothing
                , activeUser = Nothing
                , shoppingCart = []
                }
    in
        ( model
        , Cmd.batch
            [ cmd
            , getShoppingCartCmd
            , Task.attempt AccessTokenRetrieved getToken
            ]
        )


getShoppingCartCmd : Cmd Msg
getShoppingCartCmd =
    Task.attempt ShoppingCartRetrieved getShoppingCart


initialPage : Page
initialPage =
    Home (Home.model [] Nothing)



-- VIEW --


view : Model -> Html Msg
view model =
    let
        fullName =
            if isLoggedIn model then
                let
                    fullName =
                        Maybe.map (\user -> user.personalData.fullName) model.activeUser
                            |> Maybe.withDefault ""
                in
                    Just fullName
            else
                Nothing

        totalQuantityInShoppingCart =
            List.foldl (\product b -> b + (product.quantity)) 0 model.shoppingCart

        frame =
            Page.frame fullName totalQuantityInShoppingCart
    in
        case model.activePage of
            Home subModel ->
                Home.view subModel
                    |> frame
                    |> Html.map HomeMsg

            Register subModel ->
                Register.view subModel
                    |> frame
                    |> Html.map RegisterMsg

            Login subModel ->
                Login.view subModel
                    |> frame
                    |> Html.map LoginMsg

            ShoppingCart subModel ->
                let
                    user =
                        Maybe.withDefault User.empty model.activeUser

                    shoppingCartModel =
                        { shoppingCart = model.shoppingCart
                        , orderDetails = user.personalData
                        }
                in
                    ShoppingCart.view subModel
                        |> frame
                        |> Html.map ShoppingCartMsg

            NotFound ->
                NotFound.view
                    |> frame


setRoute : Maybe Route -> Model -> ( Model, Cmd Msg )
setRoute maybeRoute model =
    case maybeRoute of
        Nothing ->
            ( { model | activePage = NotFound }, Cmd.none )

        Just (Route.Home maybeAccessToken maybeMessage) ->
            let
                ( newModel, cmd ) =
                    case maybeAccessToken of
                        Just token ->
                            let
                                task =
                                    Http.toTask (getUserFromToken token)
                            in
                                ( { model | accessToken = Just token }
                                , Cmd.batch
                                    [ Task.attempt UserDetailsRetrieved task
                                    , Task.attempt AccessTokenStored (setToken token)
                                    ]
                                )

                        Nothing ->
                            ( model, Cmd.none )
            in
                newModel
                    ! [ Task.attempt HomeLoaded (Home.init maybeMessage), cmd ]

        Just Route.Register ->
            ( { model | activePage = Register Register.emptyModel }, Cmd.none )

        Just Route.StartLogin ->
            ( model, initLogin )

        Just (Route.Login (Just _) (Just redirectUri) (Just _)) ->
            ( { model | activePage = Login (Login.initialModel redirectUri) }, Cmd.none )

        Just (Route.Login _ _ _) ->
            ( { model | activePage = NotFound }, Cmd.none )

        Just Route.Logout ->
            case model.accessToken of
                Just token ->
                    let
                        tryLogout =
                            Http.toTask (logout token)
                    in
                        { model | activePage = Home Home.initialModel, accessToken = Nothing, activeUser = Nothing }
                            ! [ Task.attempt LogoutSuccess tryLogout
                              , Task.attempt ShoppingCartStored deleteShoppingCart
                              , Task.attempt AccessTokenStored deleteToken
                              ]

                Nothing ->
                    ( model, Cmd.none )

        Just Route.ShoppingCart ->
            let
                user =
                    Maybe.withDefault User.empty model.activeUser

                shoppingCartModel =
                    { shoppingCart = model.shoppingCart
                    , orderDetails = user.personalData
                    , orderFormShoutOutMsg = Nothing
                    }
            in
                ( { model | activePage = ShoppingCart shoppingCartModel }, Cmd.none )


type Msg
    = SetRoute (Maybe Route)
    | Authorize
    | UserDetailsRetrieved (Result Error User)
    | LogoutSuccess (Result Error ())
    | HomeLoaded (Result Error Home.Model)
    | RegisterLoaded (Result Error Register.Model)
    | LoginLoaded (Result Error Login.Model)
    | ShoppingCartPageLoaded (Result Error ShoppingCart.Model)
    | ShoppingCartRetrieved (Result () (Maybe ShoppingCart))
    | ShoppingCartStored (Result () ())
    | AccessTokenRetrieved (Result StorageError (Maybe AccessToken))
    | AccessTokenStored (Result StorageError ())
    | HomeMsg HomeMsgs.Msg
    | RegisterMsg RegisterMsgs.Msg
    | LoginMsg LoginMsgs.Msg
    | ShoppingCartMsg ShoppingCart.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    updatePage msg model


updatePage : Msg -> Model -> ( Model, Cmd Msg )
updatePage msg model =
    let
        toPage toModel toMsg subUpdate subMsg subModel =
            let
                ( newModel, newCmd ) =
                    subUpdate subMsg subModel
            in
                ( { model | activePage = toModel newModel }, Cmd.map toMsg newCmd )
    in
        case ( msg, model.activePage ) of
            ( SetRoute route, _ ) ->
                setRoute route model

            ( Authorize, _ ) ->
                ( model, initLogin )

            ( UserDetailsRetrieved result, _ ) ->
                case result of
                    Err er ->
                        ( { model | accessToken = Nothing }, Cmd.none )

                    Ok newUser ->
                        ( { model | activeUser = Just newUser }, Cmd.none )

            ( LogoutSuccess result, _ ) ->
                case result of
                    Err (BadStatus response) ->
                        -- If status is unauthorized, the access token is not valid anymore
                        case response.status.code of
                            401 ->
                                ( { model | accessToken = Nothing }, modifyUrl (Route.Home Nothing Nothing) )

                            _ ->
                                ( model, Cmd.none )

                    Err _ ->
                        ( model, Cmd.none )

                    Ok _ ->
                        ( { model | accessToken = Nothing }, modifyUrl (Route.Home Nothing Nothing) )

            ( HomeLoaded (Ok subModel), _ ) ->
                ( { model | activePage = (Home subModel) }, Cmd.none )

            ( HomeLoaded (Err error), _ ) ->
                ( model, Cmd.none )

            ( RegisterLoaded (Ok subModel), _ ) ->
                ( { model | activePage = (Register subModel) }, Cmd.none )

            ( RegisterLoaded (Err error), _ ) ->
                ( model, Cmd.none )

            ( LoginLoaded (Ok subModel), _ ) ->
                ( { model | activePage = (Login subModel) }, Cmd.none )

            ( LoginLoaded (Err error), _ ) ->
                ( model, Cmd.none )

            ( ShoppingCartPageLoaded (Ok subModel), _ ) ->
                ( { model | activePage = (ShoppingCart subModel) }, Cmd.none )

            ( ShoppingCartPageLoaded (Err error), _ ) ->
                ( model, Cmd.none )

            ( ShoppingCartRetrieved (Ok mShoppingCart), _ ) ->
                let
                    shoppingCart =
                        mShoppingCart
                            |> Maybe.withDefault []

                    activePage =
                        case model.activePage of
                            ShoppingCart subModel ->
                                ShoppingCart { subModel | shoppingCart = shoppingCart }

                            _ ->
                                model.activePage
                in
                    ( { model | shoppingCart = shoppingCart, activePage = activePage }, Cmd.none )

            ( ShoppingCartRetrieved (Err _), _ ) ->
                ( { model | shoppingCart = [] }, Cmd.none )

            ( AccessTokenRetrieved (Ok (Just accessToken)), _ ) ->
                let
                    task =
                        Http.toTask (getUserFromToken accessToken)
                in
                    ( { model | accessToken = Just accessToken }
                    , Task.attempt UserDetailsRetrieved task
                    )

            ( AccessTokenRetrieved (Ok Nothing), _ ) ->
                ( model, Cmd.none )

            ( AccessTokenStored (Ok _), _ ) ->
                ( model, Cmd.none )

            ( HomeMsg subMsg, Home subModel ) ->
                case subMsg of
                    AddProductToShoppingCart product ->
                        let
                            ( newModel, newCmd ) =
                                toPage Home HomeMsg (Home.update) subMsg subModel

                            newShoppingCart =
                                addProductToShoppingCart model.shoppingCart product
                        in
                            ( newModel
                            , Cmd.batch
                                [ newCmd
                                , Task.attempt ShoppingCartStored (setShoppingCart newShoppingCart)
                                ]
                            )

                    _ ->
                        toPage Home HomeMsg (Home.update) subMsg subModel

            ( ShoppingCartStored (Ok _), _ ) ->
                ( model, getShoppingCartCmd )

            ( RegisterMsg subMsg, Register subModel ) ->
                toPage Register RegisterMsg (Register.update) subMsg subModel

            ( LoginMsg subMsg, Login subModel ) ->
                toPage Login LoginMsg (Login.update) subMsg subModel

            ( ShoppingCartMsg subMsg, ShoppingCart subModel ) ->
                let
                    ( newModel, newCmd ) =
                        toPage ShoppingCart ShoppingCartMsg (ShoppingCart.update) subMsg subModel
                in
                    case subMsg of
                        RemoveProductFromShoppingCart productId ->
                            let
                                newShoppingCart =
                                    removeProductFromShoppingCart model.shoppingCart productId
                            in
                                ( newModel
                                , Cmd.batch
                                    [ newCmd
                                    , Task.attempt ShoppingCartStored (setShoppingCart newShoppingCart)
                                    ]
                                )

                        ChangeQuantityOfProductInShoppingCart productId newQuantity ->
                            let
                                newShoppingCart =
                                    changeQuantityOfProductInShoppingCart model.shoppingCart productId newQuantity
                            in
                                ( newModel
                                , Cmd.batch
                                    [ newCmd
                                    , Task.attempt ShoppingCartStored (setShoppingCart newShoppingCart)
                                    ]
                                )

                        OrderPlaced result ->
                            case result of
                                Ok _ ->
                                    newModel
                                        ! [ Task.attempt ShoppingCartStored deleteShoppingCart
                                          , newCmd
                                          ]

                                Err _ ->
                                    ( newModel, newCmd )

                        _ ->
                            ( newModel, newCmd )

            ( _, NotFound ) ->
                ( model, Cmd.none )

            ( _, _ ) ->
                ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions =
    \_ -> Sub.none


main : Program Never Model Msg
main =
    Navigation.program (Route.fixLocationQuery >> Route.fromLocation >> SetRoute)
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


isLoggedIn : Model -> Bool
isLoggedIn model =
    Maybe.withDefault False (Maybe.map (\_ -> True) model.activeUser)
