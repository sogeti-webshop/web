module Views.ShoutOutBanner exposing (view)

import Html exposing (Html, div, text, li, ul, p)
import Html.Attributes exposing (class, id)
import Data.ShoutOutMessage exposing (ShoutOutMessage, Status(Success, Danger))


view : ShoutOutMessage -> Html msg
view shoutOutMessage =
    let
        view =
            case shoutOutMessage.status of
                Success ->
                    [ p [] [ text shoutOutMessage.title ]
                    , ul [] (List.map viewItemOnShoutOut shoutOutMessage.shoutOutItems)
                    ]

                Danger ->
                    [ ul [] (List.map viewItemOnShoutOut shoutOutMessage.shoutOutItems) ]
    in
        div [ class ("shout-out-banner" ++ " " ++ (toString shoutOutMessage.status)) ]
            view


viewItemOnShoutOut : String -> Html msg
viewItemOnShoutOut item =
    li [] [ text item ]
