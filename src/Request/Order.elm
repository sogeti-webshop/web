module Request.Order exposing (orderShoppingCart)

import Http
import HttpBuilder
import Request.Settings exposing (orderServiceUrl)
import Data.Order as Order exposing (Order)


orderShoppingCart : Order.Order -> Http.Request ()
orderShoppingCart order =
    orderServiceUrl
        ++ "/order"
        |> HttpBuilder.post
        |> HttpBuilder.withJsonBody (Order.encode order)
        |> HttpBuilder.toRequest
