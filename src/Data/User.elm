module Data.User exposing (User, encode, decoder, setEmail, setPassword, setFullName, setAddress, setPostalCode, setResidence, empty)

import Data.Details exposing (PersonalData, decoderDetails)
import Json.Encode as Encode
import Json.Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (decode, required, optional)


type alias User =
    { password : String
    , personalData : PersonalData
    }


empty : User
empty =
    { password = ""
    , personalData =
        { email = ""
        , fullName = ""
        , address = ""
        , postalCode = ""
        , residence = ""
        }
    }


setEmail : String -> User -> User
setEmail newEmail user =
    let
        personalData =
            user.personalData
    in
        { user | personalData = { personalData | email = newEmail } }


setPassword : String -> User -> User
setPassword newPassword user =
    { user | password = newPassword }


setFullName : String -> User -> User
setFullName newFullName user =
    let
        personalData =
            user.personalData
    in
        { user | personalData = { personalData | fullName = newFullName } }


setAddress : String -> User -> User
setAddress newAddress user =
    let
        personalData =
            user.personalData
    in
        { user | personalData = { personalData | address = newAddress } }


setPostalCode : String -> User -> User
setPostalCode newPostalCode user =
    let
        personalData =
            user.personalData
    in
        { user | personalData = { personalData | postalCode = newPostalCode } }


setResidence : String -> User -> User
setResidence newResidence user =
    let
        personalData =
            user.personalData
    in
        { user | personalData = { personalData | residence = newResidence } }


encode : User -> Encode.Value
encode user =
    Encode.object
        [ ( "password", Encode.string user.password )
        , ( "personalData"
          , Encode.object
                [ ( "email", Encode.string user.personalData.email )
                , ( "fullName", Encode.string user.personalData.fullName )
                , ( "address", Encode.string user.personalData.address )
                , ( "postalCode", Encode.string user.personalData.postalCode )
                , ( "residence", Encode.string user.personalData.residence )
                ]
          )
        ]


decoder : Decoder User
decoder =
    decode User
        |> optional "password" string ""
        |> required "personalData" decoderDetails
