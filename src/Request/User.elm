module Request.User exposing (register, initLogin, login, getUserFromToken, logout)

import Http
import HttpBuilder
import Request.Settings exposing (apiUrl)
import Data.User as User exposing (User, encode)
import OAuth
import OAuth.Implicit
import Request.Settings exposing (clientId)
import Data.AccessToken exposing (AccessToken, accessTokenDecoder)


register : User -> Http.Request ()
register user =
    apiUrl
        ++ "/user"
        |> HttpBuilder.post
        |> HttpBuilder.withJsonBody (User.encode user)
        |> HttpBuilder.toRequest


initLogin : Cmd msg
initLogin =
    OAuth.Implicit.authorize
        { clientId = clientId
        , redirectUri = "/#/"
        , responseType = OAuth.Token -- Use the OAuth.Token response type
        , scope = []
        , state = Nothing
        , url = "/#/login"
        }


login : String -> String -> Http.Request AccessToken
login email password =
    apiUrl
        ++ "/auth"
        |> HttpBuilder.post
        |> HttpBuilder.withQueryParams
            [ ( "grant_type", "implicit" )
            , ( "client_id", clientId )
            , ( "email", email )
            , ( "password", password )
            ]
        |> HttpBuilder.withExpect (Http.expectJson accessTokenDecoder)
        |> HttpBuilder.toRequest


getUserFromToken : AccessToken -> Http.Request User
getUserFromToken token =
    apiUrl
        ++ "/user"
        |> HttpBuilder.get
        |> HttpBuilder.withQueryParams [ ( "access_token", token ) ]
        |> HttpBuilder.withExpect (Http.expectJson User.decoder)
        |> HttpBuilder.toRequest


logout : AccessToken -> Http.Request ()
logout token =
    apiUrl
        ++ "/auth"
        |> HttpBuilder.delete
        |> HttpBuilder.withQueryParams [ ( "access_token", token ) ]
        |> HttpBuilder.toRequest
