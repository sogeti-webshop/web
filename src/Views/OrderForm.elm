module Views.OrderForm exposing (emailField, fullNameField, addressField, postalCodeField, residenceField, placeOrderButton)

import Html exposing (Html, div, input, label, text, button, h3)
import Html.Attributes exposing (class, id, for, type_, required, name, value)
import Html.Events exposing (onInput, onClick)


emailField : String -> (String -> msg) -> Html msg
emailField email msg =
    div [ class "row" ]
        [ div [ class "input-field col s12" ]
            [ input [ id "email", type_ "email", onInput msg, required True, class "validate active", value email ] []
            , label [ for "email" ] [ text "E-mailadres" ]
            ]
        ]


fullNameField : String -> (String -> msg) -> Html msg
fullNameField fullName msg =
    div [ class "row" ]
        [ div [ class "input-field col s12" ]
            [ input [ id "full-name", type_ "text", onInput msg, required True, class "validate", value fullName ] []
            , label [ for "full-name" ] [ text "Volledige naam" ]
            ]
        ]


addressField : String -> (String -> msg) -> Html msg
addressField address msg =
    div [ class "row" ]
        [ div [ class "input-field col s12" ]
            [ input [ id "address", type_ "text", onInput msg, required True, class "validate", value address ] []
            , label [ for "address" ] [ text "Adres" ]
            ]
        ]


postalCodeField : String -> (String -> msg) -> Html msg
postalCodeField postalCode msg =
    div [ class "row" ]
        [ div [ class "input-field col s12" ]
            [ input [ id "postal-code", type_ "text", onInput msg, required True, class "validate", value postalCode ] []
            , label [ for "postal-code" ] [ text "Postcode" ]
            ]
        ]


residenceField : String -> (String -> msg) -> Html msg
residenceField residence msg =
    div [ class "row" ]
        [ div [ class "input-field col s12" ]
            [ input [ id "residence", type_ "text", onInput msg, required True, class "validate", value residence ] []
            , label [ for "residence" ] [ text "Woonplaats" ]
            ]
        ]


placeOrderButton : msg -> Html msg
placeOrderButton msg =
    div [ class "row" ]
        [ div [ class "col right" ]
            [ button
                [ class "btn waves-effect waves-light", name "action", onClick msg ]
                [ text "Bestel" ]
            ]
        ]
