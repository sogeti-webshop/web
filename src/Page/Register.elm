module Page.Register exposing (Model, emptyModel, view, update)

import Html exposing (Html)
import Views.Register as Register
import Data.User exposing (User, setEmail, setPassword, setFullName, setAddress, setPostalCode, setResidence)
import Request.User exposing (register)
import Http exposing (Error(..))
import Task
import Page.Register.Msg exposing (Msg(..))
import Data.ShoutOutMessage exposing (ShoutOutMessage, errorShoutOutMessage, successShoutOutMessage)
import Data.Validation exposing (decodeErrors)


type alias Model =
    { user : User
    , shoutOutBanner : Maybe ShoutOutMessage
    }


emptyModel : Model
emptyModel =
    { user =
        { password = ""
        , personalData =
            { email = ""
            , fullName = ""
            , address = ""
            , postalCode = ""
            , residence = ""
            }
        }
    , shoutOutBanner = Nothing
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateEmail newEmail ->
            ( { model | user = setEmail newEmail model.user }, Cmd.none )

        UpdatePassword newPassword ->
            ( { model | user = setPassword newPassword model.user }, Cmd.none )

        UpdateFullName newFullName ->
            ( { model | user = setFullName newFullName model.user }, Cmd.none )

        UpdateAddress newAddress ->
            ( { model | user = setAddress newAddress model.user }, Cmd.none )

        UpdatePostalCode newPostalCode ->
            ( { model | user = setPostalCode newPostalCode model.user }, Cmd.none )

        UpdateResidence newResidence ->
            ( { model | user = setResidence newResidence model.user }, Cmd.none )

        RegisterUser ->
            let
                task =
                    Http.toTask (register model.user)
            in
                ( model, Task.attempt UserRegistered task )

        UserRegistered result ->
            case result of
                Err httpError ->
                    case httpError of
                        BadStatus response ->
                            let
                                errors =
                                    decodeErrors response.body
                            in
                                ( { model | shoutOutBanner = Just (errorShoutOutMessage "De volgende fouten traden op bij het registreren" errors) }, Cmd.none )

                        -- For the scope of the project im not handling any timeout or network problems
                        _ ->
                            ( model, Cmd.none )

                Ok _ ->
                    ( { model | shoutOutBanner = Just (successShoutOutMessage "We hebben uw account succesvol verwerkt!") }, Cmd.none )


view : Model -> Html Msg
view model =
    Register.view model.user model.shoutOutBanner
