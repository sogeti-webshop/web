module Request.ShoppingCart exposing (..)

import Storage.Session as Session
import Task exposing (Task)
import Data.ShoppingCart as ShoppingCart exposing (ShoppingCart, jsonStringToShoppingCart, shoppingCartToJson)


shoppingCartKey : String
shoppingCartKey =
    "shoppingcart"



-- The mapping of the error to unit is definetily not perfect, but it's better
-- than exposing an library type of error (from the storage library) to the outside


getShoppingCart : Task () (Maybe ShoppingCart)
getShoppingCart =
    Session.get shoppingCartKey
        |> Task.map (Maybe.andThen jsonStringToShoppingCart)
        |> Task.mapError (\x -> ())


setShoppingCart : ShoppingCart -> Task () ()
setShoppingCart shoppingCart =
    let
        json =
            shoppingCartToJson shoppingCart
    in
        Session.set shoppingCartKey json
            |> Task.mapError (\x -> ())


deleteShoppingCart : Task () ()
deleteShoppingCart =
    Session.remove shoppingCartKey
        |> Task.mapError (\x -> ())
