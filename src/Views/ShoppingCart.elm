module Views.ShoppingCart exposing (isShoppingCartEmpty, continueShoppingButton, shoppingCartElement)

import Html exposing (Html, h3, p, text, div, a, i, select, option, span, hr)
import Html.Attributes exposing (id, class, attribute, value, selected, disabled)
import Html.Events exposing (onClick, onInput)
import Data.ShoppingCart exposing (ShoppingCart, ShoppingCartItem, maxQuantityPerProductAndOrder)
import Data.Product exposing (ProductId)
import Round
import Route exposing (Route(Home))


type alias DeleteProductMsg msg =
    ProductId -> msg


type alias ChangeQuantityMsg msg =
    ProductId -> Int -> msg


continueShoppingButton : Html msg
continueShoppingButton =
    a [ class "shopping-cart_continue-shopping-button btn waves-effect waves-light", Route.href (Home Nothing Nothing) ]
        [ i [ class "fa fa-2x fa-arrow-left" ] [], text "  Verder winkelen" ]


shoppingCartElement : ShoppingCart -> DeleteProductMsg msg -> ChangeQuantityMsg msg -> Html msg
shoppingCartElement shoppingCart deleteMsg changeQuantityMsg =
    let
        totalPriceOfShoppingCart =
            shoppingCart
                |> calculateShoppingCartTotalPrice
                |> floatToTwoDecimalPriceAsString
                |> text
    in
        if isShoppingCartEmpty shoppingCart then
            text "Er zitten momenteel geen producten in uw winkelwagen"
        else
            div []
                [ div [ id "shoppingCartList", class "collection" ]
                    (shoppingCartHeader :: (List.map (viewShoppingCartItem deleteMsg changeQuantityMsg) shoppingCart))
                , div [ class "shopping-cart_total-price-box" ]
                    [ div [ class "col s10 m10 l10" ] []
                    , span [ class "col s2 m2 l2 shopping-cart_total-price-box_total-price bold-label" ]
                        [ totalPriceOfShoppingCart ]
                    ]
                ]


viewShoppingCartItem : DeleteProductMsg msg -> ChangeQuantityMsg msg -> ShoppingCartItem -> Html msg
viewShoppingCartItem deleteMsg changeQuantityMsg shoppingCartItem =
    let
        pricePerProduct =
            shoppingCartItem.product.price
                |> floatToTwoDecimalPriceAsString
                |> text

        subTotal =
            shoppingCartItem.product.price
                * (toFloat shoppingCartItem.quantity)
                |> floatToTwoDecimalPriceAsString
                |> text
    in
        div [ class "row collection-item shopping-cart-item" ]
            [ i [ class "col s1 m1 l1 fa fa-2x fa-times shopping-cart-item_remove-from-cart", attribute "aria-hidden" "true", onClick (deleteMsg shoppingCartItem.product.id) ] []
            , a [ class "col s3 m3 l3 shopping-cart-item_product-title" ] [ text shoppingCartItem.product.name ]
            , div [ class "col s2 m2 l1 shopping-cart-item_product-quantity-wrapper" ]
                [ select [ class "shopping-cart-item_product-quantity-stepper", onInput (stringToInt >> (changeQuantityMsg shoppingCartItem.product.id)) ]
                    (List.map
                        (\quantity ->
                            let
                                isSelected =
                                    quantity == shoppingCartItem.quantity

                                isDisabled =
                                    False
                            in
                                option
                                    [ value (toString quantity), selected isSelected, disabled isDisabled ]
                                    [ text (toString quantity) ]
                        )
                        (List.range 1 maxQuantityPerProductAndOrder)
                    )
                ]
            , div [ class "col s1 m1 l2" ] []
            , div [ class "col s2 m2 l2" ]
                [ div [ class "shopping-cart-item_price-per-product" ]
                    [ pricePerProduct ]
                ]
            , div [ class "col s1 m1 l1" ] []
            , div [ class "col s2 m2 l2" ]
                [ div [ class "shopping-cart-item_price-per-product-subtotal" ]
                    [ subTotal ]
                ]
            ]


shoppingCartHeader : Html msg
shoppingCartHeader =
    div [ class "row collection-item shopping-cart-header" ]
        [ div [ class "col s1 m1 l1" ] []
        , div [ class "col s3 m3 l3 shopping-cart-header_title bold-label" ] [ text "Titel" ]
        , div [ class "col s2 m2 l1 shopping-cart-header_quantity bold-label" ] [ text "  Aantal:" ]
        , div [ class "col s1 m1 l2" ] []
        , div [ class "col s2 m2 l2 shopping-cart-header_price-per-product bold-label" ] [ text "Per stuk:" ]
        , div [ class "col s1 m1 l1" ] []
        , div [ class "col s2 m2 l2 shopping-cart-header_subtotal bold-label" ] [ text "Subtotaal:" ]
        ]


calculateShoppingCartTotalPrice : ShoppingCart -> Float
calculateShoppingCartTotalPrice shoppingCart =
    List.foldl
        (\item b ->
            item.quantity
                |> toFloat
                |> (*) item.product.price
                |> (+) b
        )
        (toFloat 0)
        shoppingCart


floatToTwoDecimalPriceAsString : Float -> String
floatToTwoDecimalPriceAsString float =
    float
        |> Round.round 2
        |> String.append "€"


isShoppingCartEmpty : ShoppingCart -> Bool
isShoppingCartEmpty shoppingCart =
    (List.length shoppingCart) == 0


stringToInt : String -> Int
stringToInt string =
    String.toInt string
        |> Result.withDefault 1
