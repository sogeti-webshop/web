module Request.Storage exposing (..)

import Task exposing (Task)
import Storage.Error exposing (Error)
import Storage.Session as Session
import Json.Encode as Encode exposing (encode)
import Json.Decode as Decode exposing (Decoder, decodeString)


type StorageError
    = StorageError String


type alias StorageKey =
    String


get : StorageKey -> Decoder a -> Task StorageError (Maybe a)
get key decoder =
    Session.get key
        |> Task.mapError mapErrorToStorageError
        |> Task.map (Maybe.map (\json -> decodeString decoder json))
        |> Task.map (Maybe.andThen Result.toMaybe)


set : StorageKey -> Encode.Value -> Task StorageError ()
set key data =
    let
        json =
            encode 0 data
    in
        Session.set key json
            |> Task.mapError mapErrorToStorageError


delete : StorageKey -> Task StorageError ()
delete key =
    Session.remove key
        |> Task.mapError mapErrorToStorageError


mapErrorToStorageError : Error -> StorageError
mapErrorToStorageError error =
    StorageError (toString error)
