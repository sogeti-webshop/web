module Views.Register exposing (view)

import Html exposing (Html, Attribute, div, input, text, form, label, button, i)
import Html.Attributes exposing (class, placeholder, id, type_, for, required, name, value)
import Html.Events exposing (onClick, onInput, onWithOptions)
import Data.User exposing (User)
import Data.ShoutOutMessage exposing (ShoutOutMessage)
import Page.Register.Msg exposing (Msg(..))
import Views.ShoutOutBanner as ShoutOutBanner


view : User -> Maybe ShoutOutMessage -> Html Msg
view user shoutOutMessage =
    let
        shoutOutBanner =
            case shoutOutMessage of
                Just message ->
                    ShoutOutBanner.view message

                Nothing ->
                    text ""
    in
        div [ id "register-form", class "row" ]
            [ shoutOutBanner
            , div [ class "col s12 m7 l6" ]
                [ div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ id "email", type_ "email", required True, class "validate", onInput UpdateEmail, value user.personalData.email ] []
                        , label [ for "email" ] [ text "E-mailadres" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ id "password", type_ "password", required True, class "validate", onInput UpdatePassword, value user.password ] []
                        , label [ for "password" ] [ text "Wachtwoord" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ id "full-name", type_ "text", required True, class "validate", onInput UpdateFullName, value user.personalData.fullName ] []
                        , label [ for "full-name" ] [ text "Volledige naam" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ id "address", type_ "text", required True, class "validate", onInput UpdateAddress, value user.personalData.address ] []
                        , label [ for "address" ] [ text "Adres" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ id "postal-code", type_ "text", required True, class "validate", onInput UpdatePostalCode, value user.personalData.postalCode ] []
                        , label [ for "postal-code" ] [ text "Postcode" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "input-field col s12" ]
                        [ input [ id "residence", type_ "text", required True, class "validate", onInput UpdateResidence, value user.personalData.residence ] []
                        , label [ for "residence" ] [ text "Woonplaats" ]
                        ]
                    ]
                , div [ class "row" ]
                    [ div [ class "col right" ]
                        [ button
                            [ class "register-form_register-button btn waves-effect waves-light", name "action", onClick RegisterUser ]
                            [ text "Registreer" ]
                        ]
                    ]
                ]
            ]
