module Data.AccessToken exposing (AccessToken, accessTokenDecoder, encodeAccessToken)

import Json.Decode exposing (Decoder, string, decodeString, field)
import Json.Encode as Encode


type alias AccessToken =
    String



-- this decoder assumes that the access_token is part of an js object, instead of just a plain json value


accessTokenDecoder : Decoder AccessToken
accessTokenDecoder =
    field "accessToken" string


encodeAccessToken : AccessToken -> Encode.Value
encodeAccessToken token =
    Encode.string token
