# Sogeti SPA

This single page application is build as POC for the graduation of Mauro Palsgraaf at the HAN University of applied sciences. It's build using ELM as the frontend technology.

## Development

Run `yarn install` in the root of this project to install dependencies.

To run this project, make sure webpack is installed globally or use the binary in the node_modules/.bin installed with yarn

webpack requires the elm dependencies to be present. It uses an alternative package manager, that allows you to pull unofficial published packages to the elm-repository. The elm github installer can be found here [elm-github-installer](https://github.com/gdotdesign/elm-github-install). Make sure this is installed globally. Then, run `elm-install`.

Run webpack to compile all code and styling once. Run webpack -w to also watch files and let it check for changes. It will auto-compile when changes are detected.

To actually see the changes in the browser, you need to start a docker container that has a volume mapping on the compiled code. Run `docker-compose up` to start a Nginx instance. The application can now be reached at ${DOCKER_HOST}:8080 where DOCKER_HOST is the host where docker runs. On linux and osx this is most likely localhost. When using docker-machine, this could be different.

## ELM

This project uses ELM for building the single-page application. Elm compiles to Javascript, but because of it's amazing type system and simplicity it allows developers to build more robust applications. More about ELM can be found here: [elm-lang](http://elm-lang.org "Elm homepage")
