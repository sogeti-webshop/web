module Page.Register.Msg exposing (Msg, Msg(..))

import Http


type Msg
    = UpdateEmail String
    | UpdatePassword String
    | UpdateFullName String
    | UpdateAddress String
    | UpdatePostalCode String
    | UpdateResidence String
    | RegisterUser
    | UserRegistered (Result Http.Error ())
