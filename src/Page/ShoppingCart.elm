module Page.ShoppingCart exposing (Model, Msg(..), update, view)

import Html exposing (Html, h3, hr, text, div)
import Html.Attributes exposing (id, class)
import Views.ShoppingCart exposing (isShoppingCartEmpty, continueShoppingButton, shoppingCartElement)
import Views.OrderForm exposing (emailField, fullNameField, addressField, postalCodeField, residenceField, placeOrderButton)
import Views.ShoutOutBanner as ShoutOutBanner
import Data.Details exposing (OrderDetails)
import Data.ShoppingCart exposing (ShoppingCart)
import Data.Product exposing (ProductId)
import Data.ShoutOutMessage exposing (ShoutOutMessage, errorShoutOutMessage)
import Data.Validation exposing (decodeErrors)
import Request.Order exposing (orderShoppingCart)
import Http exposing (Error(BadStatus))
import Task
import Route exposing (modifyUrl, Route(Home))


type alias Model =
    { shoppingCart : ShoppingCart
    , orderDetails : OrderDetails
    , orderFormShoutOutMsg : Maybe ShoutOutMessage
    }


model : ShoppingCart -> OrderDetails -> Model
model shoppingCart orderDetails =
    { shoppingCart = shoppingCart
    , orderDetails = orderDetails
    , orderFormShoutOutMsg = Nothing
    }


type Msg
    = ChangeQuantityOfProductInShoppingCart ProductId Int
    | RemoveProductFromShoppingCart ProductId
    | UpdateEmail String
    | UpdateFullName String
    | UpdateAddress String
    | UpdatePostalCode String
    | UpdateResidence String
    | PlaceOrder
    | OrderPlaced (Result Http.Error ())


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeQuantityOfProductInShoppingCart productId newQuantity ->
            -- Already handled at the top component
            ( model, Cmd.none )

        RemoveProductFromShoppingCart productId ->
            -- Already handled at the top component
            ( model, Cmd.none )

        UpdateEmail newEmail ->
            let
                orderDetails =
                    model.orderDetails
            in
                ( { model | orderDetails = { orderDetails | email = newEmail } }, Cmd.none )

        UpdateFullName newFullName ->
            let
                orderDetails =
                    model.orderDetails
            in
                ( { model | orderDetails = { orderDetails | fullName = newFullName } }, Cmd.none )

        UpdateAddress newAddress ->
            let
                orderDetails =
                    model.orderDetails
            in
                ( { model | orderDetails = { orderDetails | address = newAddress } }, Cmd.none )

        UpdatePostalCode newPostalCode ->
            let
                orderDetails =
                    model.orderDetails
            in
                ( { model | orderDetails = { orderDetails | postalCode = newPostalCode } }, Cmd.none )

        UpdateResidence newResidence ->
            let
                orderDetails =
                    model.orderDetails
            in
                ( { model | orderDetails = { orderDetails | residence = newResidence } }, Cmd.none )

        PlaceOrder ->
            let
                placeOrderCmd =
                    Http.toTask (orderShoppingCart { orderDetails = model.orderDetails, shoppingCart = model.shoppingCart })
            in
                ( model, Task.attempt OrderPlaced placeOrderCmd )

        OrderPlaced result ->
            case result of
                Ok _ ->
                    ( model, Route.modifyUrl (Home Nothing (Just "Bedankt voor uw bestelling!")) )

                Err httpError ->
                    case httpError of
                        BadStatus response ->
                            let
                                errors =
                                    decodeErrors response.body
                            in
                                ( { model | orderFormShoutOutMsg = Just (errorShoutOutMessage "Er is iets mis gegaan bij het plaatsen van een bestelling" errors) }, Cmd.none )

                        -- For the scope of the project im not handling any timeout or network problems
                        _ ->
                            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    let
        orderForm =
            if isShoppingCartEmpty model.shoppingCart then
                div [] []
            else
                viewOrderForm model
    in
        div []
            [ div [ class "row" ]
                [ h3 [] [ text "Winkelwagen" ] ]
            , div [ class "row" ]
                [ continueShoppingButton
                ]
            , div [ class "shopping-cart row" ]
                [ shoppingCartElement model.shoppingCart RemoveProductFromShoppingCart ChangeQuantityOfProductInShoppingCart
                ]
            , hr [] []
            , div [ class "order-form row" ]
                [ orderForm
                ]
            ]


viewOrderForm : Model -> Html Msg
viewOrderForm model =
    let
        shoutOutBannerView =
            Maybe.map (ShoutOutBanner.view) model.orderFormShoutOutMsg
                |> Maybe.withDefault (text "")
    in
        div [ class "col s12 m7 l6" ]
            [ h3 [] [ text "Plaats bestelling" ]
            , div [ class "order-form__shout-out-banner" ]
                [ shoutOutBannerView ]
            , emailField model.orderDetails.email UpdateEmail
            , fullNameField model.orderDetails.fullName UpdateFullName
            , addressField model.orderDetails.address UpdateAddress
            , postalCodeField model.orderDetails.postalCode UpdatePostalCode
            , residenceField model.orderDetails.residence UpdateResidence
            , placeOrderButton PlaceOrder
            ]
