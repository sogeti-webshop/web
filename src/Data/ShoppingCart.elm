module Data.ShoppingCart exposing (..)

import Data.Product as Product exposing (Product, ProductId)
import Json.Decode as Decode exposing (Decoder, string, int, list, decodeString)
import Json.Decode.Pipeline exposing (decode, required)
import Json.Encode as Encode exposing (encode, Value)
import Result exposing (toMaybe)


type alias ShoppingCart =
    List ShoppingCartItem


type alias ShoppingCartItem =
    { product : Product, quantity : Int }


maxQuantityPerProductAndOrder : Int
maxQuantityPerProductAndOrder =
    50


addProductToShoppingCart : ShoppingCart -> Product -> ShoppingCart
addProductToShoppingCart shoppingCart product =
    let
        isProductAlreadyInShoppingCart =
            List.any
                (\shoppingCartItem -> shoppingCartItem.product.id == product.id)
                shoppingCart

        newShoppingCart =
            if isProductAlreadyInShoppingCart then
                List.map
                    (\shoppingCartItem ->
                        if shoppingCartItem.product.id == product.id then
                            if shoppingCartItem.quantity < maxQuantityPerProductAndOrder then
                                { product = shoppingCartItem.product, quantity = shoppingCartItem.quantity + 1 }
                            else
                                shoppingCartItem
                        else
                            shoppingCartItem
                    )
                    shoppingCart
            else
                shoppingCart ++ [ { product = product, quantity = 1 } ]
    in
        newShoppingCart


removeProductFromShoppingCart : ShoppingCart -> ProductId -> ShoppingCart
removeProductFromShoppingCart shoppingCart productId =
    List.filter (\shoppingCartItem -> shoppingCartItem.product.id /= productId) shoppingCart


changeQuantityOfProductInShoppingCart : ShoppingCart -> ProductId -> Int -> ShoppingCart
changeQuantityOfProductInShoppingCart shoppingCart productId newQuantity =
    List.map
        (\shoppingCartItem ->
            if shoppingCartItem.product.id == productId then
                { product = shoppingCartItem.product, quantity = newQuantity }
            else
                shoppingCartItem
        )
        shoppingCart


jsonStringToShoppingCart : String -> Maybe ShoppingCart
jsonStringToShoppingCart json =
    decodeString decoder json
        |> toMaybe


shoppingCartToJson : ShoppingCart -> String
shoppingCartToJson shoppingCart =
    encode 0 (encodeShoppingCart shoppingCart)


decoder : Decoder ShoppingCart
decoder =
    list decodeShoppingCartItem


decodeShoppingCartItem : Decoder ShoppingCartItem
decodeShoppingCartItem =
    decode ShoppingCartItem
        |> required "product" Product.decoder
        |> required "quantity" int


encodeShoppingCart : ShoppingCart -> Value
encodeShoppingCart shoppingCart =
    Encode.list (List.map encodeShoppingCartItem shoppingCart)


encodeShoppingCartItem : ShoppingCartItem -> Value
encodeShoppingCartItem shoppingCartItem =
    Encode.object
        [ ( "product", Product.encode shoppingCartItem.product )
        , ( "quantity", Encode.int shoppingCartItem.quantity )
        ]


encodeShoppingCartProductIdsAndQuantities : ShoppingCart -> Value
encodeShoppingCartProductIdsAndQuantities shoppingCart =
    let
        encodeItem =
            (\shoppingCartItem ->
                Encode.object
                    [ ( "productId", Encode.int shoppingCartItem.product.id )
                    , ( "quantity", Encode.int shoppingCartItem.quantity )
                    ]
            )
    in
        Encode.list (List.map encodeItem shoppingCart)
