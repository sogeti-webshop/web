FROM nginx:latest

COPY ./dist /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/sites-enabled/default

CMD ["nginx", "-g", "daemon off;"]
